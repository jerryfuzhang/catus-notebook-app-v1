# Catus Notebook
An elegant and effective tool that is a 'notes as todos' note taking software.


## Project setup

- Firstly install all dependencies

	```shell
	yarn install
	```

- Compiles and hot-reloads for development

	```shell
	yarn electron:serve
	```

- Compiles and minifies for production

	```shell
	yarn electron:build
	```



